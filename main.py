#! /usr/bin/python3

'''

****************DISCLAIMER**********************
I am learning python, best programming practices, and do not know the best security measures to store passwords.
I made the program as a learning experience if it doesn't follow the best security standards please enlighten me.
Do not trust this program to securely store your information!!!!!!!!!!


------ pwm ------
password manager: This program creates strong passwords. stores the username, password, service combination and encrypts all of them.

'''
import sys
import random
import bcrypt
import sqlite3
import os
import base64

print('''
        ________  _  _______  
        \____ \ \/ \/ /     \ 
        |  |_> >     /  Y Y  \  
        |   __/ \/\_/|__|_|  /
        |__|               \/ 
            Password Manager
            

Welcome to pwm!
''')




def MainMenu():

    mainMenu = input('''
    Main Menu:
        1: Create Password
        2: View Stored Passwords
        3: Store New Password
        4: Exit
    ''')

    if mainMenu == '1':
        p = Passwd.MakePass()
        MainMenu()

    elif mainMenu == '2':
        ViewPasswords()

    elif mainMenu == '3':
        StoreNewPassword()

    elif mainMenu == '4':
        print('Thanks for this  using pwm!')
        sys.exit()

    else:
        print('Not a choice Try again')


def DBCheck():
    # checks to see if 'data/db.sqlite file is there and if not then creates it.
    # then calls MakeDB() to create the database
    dbFile = 'data/db.sqlite'
    dbFilePath = os.path.exists('data')
    if dbFilePath:
        if dbFile:
            MakeDB()
        else:
            open('data/db.sqlite', 'a').close()
            MakeDB()
        MasterCheck()
    else:
        os.makedirs('data')
        open('data/db.sqlite', 'a').close()
        MakeDB()
        MasterCheck()

    return


def MakeDB():
    # checks to see if db is created or not then calls to check for master pw.
    db = sqlite3.connect('data/db.sqlite')
    cursor = db.cursor()
    # pull all info from table
    # if table exists return
    # else make table
    try:
        cursor.execute('SELECT * FROM main')
    except sqlite3.OperationalError:
        cursor.execute('''
            CREATE TABLE main(id INTEGER PRIMARY KEY AUTOINCREMENT, service INTEGER, username INTEGER, password INTEGER)
        ''')
    else:
        return

    db.commit()
    db.close()
    print('Database created')
    return


def MasterCheck():
    # checks to see if master pass is stored
    # if it's not stored then make one and store it
    db = sqlite3.connect('data/db.sqlite')
    cursor = db.cursor()
    # if master password is set then return if not then make master pass
    cursor.execute('SELECT * FROM main WHERE {idf}={my_id}'.format(idf='id', my_id=1))
    id_exists = cursor.fetchone()
    if id_exists:
        return
    else:
        print('There is no master password set.')
        setMaster = input('Set mater password: 8+ ')
        if len(setMaster) < 8:
            print('There is no master password set.')
            setMaster = input('Set master password: 8+ ')

        enpw = setMaster.encode()
        salt = bcrypt.gensalt()
        hashed = bcrypt.hashpw(enpw, salt)
        cursor.execute('''INSERT INTO main (id, service, username, password)
                         VALUES(NULL, ?, ?, ?)''', (1, 1, hashed))

    db.commit()
    db.close()
    return

class Passwd:
    @staticmethod
    def MakePass():
        # creates secure password
        passlen = input('How long does the password need to be? 8+ ')

        if passlen == '' or passlen < 8:
            passlen = 8

        s = "abcdefghijklmnopqrstuvwxyz01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()?"
        p = "".join(random.sample(s, passlen))
        print('Your new password is: ' + p)
        return p


    def goBack():
        # option to go back to main menu.
        # should be at top of every submenu option
        back = input('Would you like to go back to the main menu? y/n ')

        if back == 'y' or back == 'Y':
            MainMenu()
        elif back == 'n' or back == 'N':
            return
        else:
            print('Try again!')
            Passwd.goBack()


def login():
    # gets user input for master password and then check against the stored hash

    test_pass = input('Please enter your password: ')
    en_tp = test_pass.encode()
    db = sqlite3.connect('data/db.sqlite')
    cursor = db.cursor()

    cursor.execute('''
        SELECT * FROM MAIN WHERE service=master 
    ''')
    hashed_pass = cursor.fetchone()
    if bcrypt.checkpw(en_tp, hashed_pass[3]):
        db.close()
        return
    else:
        print('Try again!')
        login()
    db.close()

def ViewPasswords():
    # get stored passwords from db.
    # unencode them and print them to screen
    # ask what service

    print('View your passwords')
    Passwd.goBack()
    service = input('What service would you like to see: ')

    en_service = service.encode()
    b64_service = base64.standard_b64encode(en_service)
    db = sqlite3.connect('data/db.sqlite')
    c = db.cursor()
    # asd
    c.execute('SELECT * FROM main WHERE service=?', (b64_service,))
    vals = c.fetchone()

    b64_serv = base64.standard_b64decode(vals[1])
    b64_username = base64.standard_b64decode(vals[2])
    b64_passwd = base64.standard_b64decode(vals[3])

    de_serv = b64_serv.decode()
    de_username = b64_username.decode()
    de_password = b64_passwd.decode()

    print('''
        Service: %s
        Username: %s
        Password: %s
    '''% (de_serv, de_username, de_password))

    another = input('Would you like to see another service? y/n ')

    if another == 'y' or another == 'Y':
        ViewPasswords()
    elif another == 'n' or another == 'N':
        MainMenu()
    else:
        MainMenu()


def StoreNewPassword():
    # get the credentials from user and store them in data base with new id number
    print('Store new password:')
    Passwd.goBack()
    service = input('Please enter the service: ')
    username = input('Please enter the username: ')
    passwd = input('Please enter the password: ')

    en_service = service.encode()
    en_username = username.encode()
    en_passwd = passwd.encode()

    b64_service = base64.standard_b64encode(en_service)
    b64_username = base64.standard_b64encode(en_username)
    b64_password = base64.standard_b64encode(en_passwd)

    '''
    TODO:
        2) Ensure that db can't be fucked with by sql injections
        3) Research other sql hacks and secure db that way
        4) Drink all the booze
        5) Hack all the things
        6) Didn't sanitize commandlines, landmine
    '''


    db = sqlite3.connect('data/db.sqlite')
    c = db.cursor()

    c.execute('''
                INSERT INTO main (id, service, username, password)
                VALUES(NULL, ?, ?, ?)''', (b64_service, b64_username, b64_password))

    db.commit()
    db.close()
    print('Your account information is stored!')
    MainMenu()


def DeletePassword():
    # asddd
    # asks user for service to delete
    # asks for master password
    # deletes info if everything matches.

    print('Delete passwords')
    Passwd.goBack()
    # this is really janky. Can it be cleaned up?

    choice = input('Which service would you like to delete? ')
    en_choice = choice.encode()
    b64_choice = base64.standard_b64encode(en_choice)

    db = sqlite3.connect('data/db.sqlite')
    c = db.cursor()
    c.execute('DELETE FROM main WHERE service=?', (b64_choice,))
    print(choice + 'was deleted from the database!')
    db.commit()
    db.close()
    MainMenu()

login()
DBCheck()
